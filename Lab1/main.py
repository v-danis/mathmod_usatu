import matplotlib.pyplot as plt
import numpy as np
from numpy.core._multiarray_umath import (
    add_docstring, implement_array_function, _get_implementing_args)
def Logistic(x0,r):
    x_next = x0
    eps = 1.e-5
    count = 0
    for i in range(0,2000):
        x_prev = x_next
        x_next = r*x_prev*(1-x_prev)
    x_first = x_next
    while True:
        count+=1
        if(count > 100):
            x_first = x_next
            count = 0
        #print(r,' ',x_next, "KEKE")
        x_prev = x_next
        x_next = r*x_prev*(1-x_prev)
        if(abs(x_first-x_next) < eps):
            break
    for i in range(0,count+1):
        x_prev = x_next
        x_next = r*x_prev*(1-x_prev)
        if count == 3:
            print(r,x_next,count,sep='\t')


def LogisticMap():
    mu = np.arange(2, 4, 0.0001)
    x = 0.2  # Первоначальный значение
    iters = 1000  # Количество итераций без вывода
    last = 100  # Количество итераций для отрисовки результата в конце
    for i in range(iters+last):
        x = mu * x * (1 - x)
        if i >= iters:
            plt.plot(mu, x, ',k', alpha=0.25)  # alphaSet прозрачность
    plt.show()

if __name__ == "__main__":
    x0 = 0.01
    r_start = 1
    r_end = 4
    h = 0.01
    N = int((r_end - r_start)/h+1)
    for i in range(0,N):
        r = r_start+i*h
        Logistic(x0,r)
    LogisticMap()