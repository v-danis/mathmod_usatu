width=15
set key left
set xrange [-600:600]
set boxwidth width
set style fill solid 1.00 noborder
set grid
plot 'tracey1000.txt' u ($1):($2) w boxes title 'N=1000' lt rgb "#ff6200", 'tracey10000.txt' u ($1):($2) w boxes title 'N=10000' lt rgb "#38b6c7", 'tracey20000.txt' u ($1):($2) w boxes title 'N=20000' lt rgb "#af0ceb"
pause 50