#include <iostream>
#include <fstream>
#include <string>
#include <random>
#include <cmath>
#include <algorithm>
#include <vector>
#include <iterator>
#include <time.h>
double l_func(double r)
{
	return -((pow(-r + 1, 0.5) - 1) /pow(-r + 1, 0.5));
}


//void Check()
//{
//	int k = 100;
//	int N = 1e+8;
//	double l = 0.1;
//	double x, r;
//	double* hyst = new double[k];
//
//	for (int i = 0; i < k; i++) hyst[i] = 0;
//
//	srand(time(NULL));
//	for (int i = 0; i < N; i++)
//	{
//		r = dis(gen);
//		x = l_func(r);
//		hyst[(int)floor(x / l)]++;
//	}
//	std::ofstream fout("hyst.txt");
//	for (int i = 0; i < k; i++)
//		fout << l * i << " " << hyst[i] / N / l << "\n";
//	//fout << hyst[i] << "\n";
//
//	fout.close();
//
//	delete[] hyst;
//}

void Function()
{
	int t = 2000;
	int M = 10000;
	int N = 20000;
	double w = 0.5;
	int a;
	double x, y, r, l;

	double* hyst_xp = new double[t];
	double* hyst_yp = new double[t];
	double* hyst_xm = new double[t];
	double* hyst_ym = new double[t];

	for (int i = 0; i < t; i++)
	{
		hyst_xp[i] = 0;
		hyst_yp[i] = 0;
		hyst_xm[i] = 0;
		hyst_ym[i] = 0;
	}


	srand(time(NULL));
	std::random_device rd;
	//mt19937 gen;
	std::minstd_rand gen;
	gen.seed(0);
	std::uniform_real_distribution<> dis(0.00, 0.999);

	for (int k = 0; k < M; k++)
	{
		x = 0; y = 0;
		for (int i = 0; i < N; i++)
		{
			a = rand() % 4 + 1;
			
			r = dis(gen);
			
			l = l_func(r);
			if (a == 1) x += l;
			if (a == 2) y += l;
			if (a == 3) x -= l;
			if (a == 4) y -= l;

		}

		if (x >= 0)
		{
			if ((int)floor(x / w) >= t) hyst_xp[t - 1]++;
			else hyst_xp[(int)floor(x / w)]++;
		}
		else
		{
			if ((int)floor(-x / w) >= t) hyst_xm[t - 1]++;
			else hyst_xm[(int)floor(-x / w)]++;
		}

		if (y >= 0)
		{
			if ((int)floor(y / w) >= t) hyst_yp[t - 1]++;
			else hyst_yp[(int)floor(y / w)]++;
		}
		else
		{
			if ((int)floor(-y / w) >= t) hyst_ym[t - 1]++;
			else hyst_ym[(int)floor(-y / w)]++;
		}
	}

	std::ofstream xout("fx.txt");
	std::ofstream yout("fy.txt");

	double h_x = 0, h_y = 0;

	for (int i = t - 1; i >= 0; i--)
	{
		h_x = h_x + (double)hyst_xm[i] / M;
		xout << -w * i << " " << h_x << "\n";

		h_y = h_y + (double)hyst_ym[i] / M;
		yout << -w * i << " " << h_y << "\n";
	}

	for (int i = 0; i < t; i++)
	{
		h_x = h_x + (double)hyst_xp[i] / M;
		xout << w * i << " " << h_x << "\n";

		h_y = h_y + (double)hyst_yp[i] / M;
		yout << w * i << " " << h_y << "\n";
	}

	xout.close();
	yout.close();

	delete[] hyst_xp;
	delete[] hyst_yp;
	delete[] hyst_xm;
	delete[] hyst_ym;
}

void Trace()
{
	int t = 2000;
	int M = 30000;
	int N = 1000;
	double w = 15;
	int a;
	double x, y, r, l;

	double* hyst_xp = new double[t];
	double* hyst_yp = new double[t];
	double* hyst_xm = new double[t];
	double* hyst_ym = new double[t];

	for (int i = 0; i < t; i++)
	{
		hyst_xp[i] = 0;
		hyst_yp[i] = 0;
		hyst_xm[i] = 0;
		hyst_ym[i] = 0;
	}

	srand(time(NULL));
	std::random_device rd;
	//mt19937 gen;
	std::minstd_rand gen;
	gen.seed(0);
	std::uniform_real_distribution<> dis(0.00, 0.999);

	for (int k = 0; k < M; k++)
	{
		x = 0; y = 0;
		for (int i = 0; i < N; i++)
		{
			a = rand() % 4 + 1;
			r = dis(gen);
			l = l_func(r);
			if (a == 1) x += l;
			if (a == 2) y += l;
			if (a == 3) x -= l;
			if (a == 4) y -= l;
		}

		if (x >= 0)
		{
			if ((int)floor(x / w) >= t) hyst_xp[t - 1]++;
			else hyst_xp[(int)floor(x / w)]++;
		}
		else
		{
			if ((int)floor(-x / w) >= t) hyst_xm[t - 1]++;
			else hyst_xm[(int)floor(-x / w)]++;
		}

		if (y >= 0)
		{
			if ((int)floor(y / w) >= t) hyst_yp[t - 1]++;
			else hyst_yp[(int)floor(y / w)]++;
		}
		else
		{
			if ((int)floor(-y / w) >= t) hyst_ym[t - 1]++;
			else hyst_ym[(int)floor(-y / w)]++;
		}
	}

	std::ofstream xout1("tracex" + std::to_string(N) + ".txt");
	std::ofstream yout1("tracey" + std::to_string(N) + ".txt");

	for (int i = t - 1; i >= 0; i--)
	{
		xout1 << -w * i << " " << hyst_xm[i] / M << "\n";
		yout1 << -w * i << " " << hyst_ym[i] / M << "\n";
	}

	for (int i = 0; i < t; i++)
	{
		xout1 << w * i << " " << hyst_xp[i] / M << "\n";
		yout1 << w * i << " " << hyst_yp[i] / M << "\n";
	}

	xout1.close();
	yout1.close();

	N = 10000;

	for (int k = 0; k < M; k++)
	{
		x = 0; y = 0;
		for (int i = 0; i < N; i++)
		{
			a = rand() % 4 + 1;
			r = dis(gen);
			l = l_func(r);
			if (a == 1) x += l;
			if (a == 2) y += l;
			if (a == 3) x -= l;
			if (a == 4) y -= l;

		}

		if (x >= 0)
		{
			if ((int)floor(x / w) >= t) hyst_xp[t - 1]++;
			else hyst_xp[(int)floor(x / w)]++;
		}
		else
		{
			if ((int)floor(-x / w) >= t) hyst_xm[t - 1]++;
			else hyst_xm[(int)floor(-x / w)]++;
		}

		if (y >= 0)
		{
			if ((int)floor(y / w) >= t) hyst_yp[t - 1]++;
			else hyst_yp[(int)floor(y / w)]++;
		}
		else
		{
			if ((int)floor(-y / w) >= t) hyst_ym[t - 1]++;
			else hyst_ym[(int)floor(-y / w)]++;
		}
	}

	std::ofstream xout2("tracex" + std::to_string(N) + ".txt");
	std::ofstream yout2("tracey" + std::to_string(N) + ".txt");

	for (int i = t - 1; i >= 0; i--)
	{
		xout2 << -w * i << " " << hyst_xm[i] / M << "\n";
		yout2 << -w * i << " " << hyst_ym[i] / M << "\n";
	}

	for (int i = 0; i < t; i++)
	{
		xout2 << w * i << " " << hyst_xp[i] / M << "\n";
		yout2 << w * i << " " << hyst_yp[i] / M << "\n";
	}

	xout2.close();
	yout2.close();

	N = 20000;

	for (int k = 0; k < M; k++)
	{
		x = 0; y = 0;
		for (int i = 0; i < N; i++)
		{
			a = rand() % 4 + 1;
			r = dis(gen);
			l = l_func(r);
			if (a == 1) x += l;
			if (a == 2) y += l;
			if (a == 3) x -= l;
			if (a == 4) y -= l;

		}

		if (x >= 0)
		{
			if ((int)floor(x / w) >= t) hyst_xp[t - 1]++;
			else hyst_xp[(int)floor(x / w)]++;
		}
		else
		{
			if ((int)floor(-x / w) >= t) hyst_xm[t - 1]++;
			else hyst_xm[(int)floor(-x / w)]++;
		}

		if (y >= 0)
		{
			if ((int)floor(y / w) >= t) hyst_yp[t - 1]++;
			else hyst_yp[(int)floor(y / w)]++;
		}
		else
		{
			if ((int)floor(-y / w) >= t) hyst_ym[t - 1]++;
			else hyst_ym[(int)floor(-y / w)]++;
		}
	}

	std::ofstream xout3("tracex" + std::to_string(N) + ".txt");
	std::ofstream yout3("tracey" + std::to_string(N) + ".txt");

	for (int i = t - 1; i >= 0; i--)
	{
		xout3 << -w * i << " " << hyst_xm[i] / M << "\n";
		yout3 << -w * i << " " << hyst_ym[i] / M << "\n";
	}

	for (int i = 0; i < t; i++)
	{
		xout3 << w * i << " " << hyst_xp[i] / M << "\n";
		yout3 << w * i << " " << hyst_yp[i] / M << "\n";
	}

	xout3.close();
	yout3.close();


	std::ofstream fout("trace.txt");

	x = 0; y = 0;
	fout << x << " " << y << "\n";
	for (int i = 0; i < N; i++)
	{
		a = rand() % 4 + 1;
		r = dis(gen);
		l = l_func(r);
		if (a == 1) x += l;
		if (a == 2) y += l;
		if (a == 3) x -= l;
		if (a == 4) y -= l;

		fout << x << " " << y << "\n";
	}

	fout.close();

	delete[] hyst_xp;
	delete[] hyst_yp;
	delete[] hyst_xm;
	delete[] hyst_ym;
}

void MNK(int steps, int p, double* R, double N0)
{
	int N = N0;

	double sum_x = 0., sum_y = 0., sum_xx = 0., sum_xy = 0.;
	double k = 0.;
	for (int i = 0; i < p; i++)
	{
		//k = steps * i;
		//sum_x = sum_x + std::log(N + k);
		//sum_y = sum_y + std::log(R[i]);
		//sum_xx = sum_xx + std::log(N + k) * std::log(N + k);
		//sum_xy = sum_xy + std::log(R[i]) * std::log(N + k);
		k = pow(10, i);
		sum_x = sum_x + log(N*k);
		sum_y = sum_y + log(R[i]);
		sum_xx = sum_xx + log(N * k) * log(N * k);
		sum_xy = sum_xy + log(R[i]) * log(N * k);
	}
	double a1 = (p * sum_xy - sum_x * sum_y) / (p * sum_xx - sum_x * sum_x);
	double a0 = (sum_y / p) - ((sum_x * a1) / p);
	double nu = a1;
	double A = exp(a0);

	std::cout << "nu = " << nu << "\n";
	std::cout << "A = " << A << "\n";

	std::ofstream mnk("apprx.txt");
	mnk << 0 << " " << 0 << "\n";
	for (int j = 0; j < p; j++)
	{
		k = steps * j;
		mnk << N << " " << A * pow(N + k, nu) << "\n";
	}
	mnk.close();
}

int main()
{
	int steps = 10000;
	int p = 4;
	int N_start = 10;
	int N = N_start;
	int M = 10000;

	//int steps = 1500;
	//int p = 9;
	//int N_start = 1500;
	//int N = N_start;
	//int M = 1000;

	double* R = new double[p];
	//Check();
	//std::cout << "Check progress...\n";
	//Trace();
	//std::cout << "Trace progress...\n";
	//Function();
	//std::cout << "Function progress...\n";

	//int segment = 30;
	//int M = 1;
	//int N = 1e6;
	//double w = 15;
	//double x, y;
	int a;
	double r, l;
	//std::vector<double> l_arr(N);


	//srand(time(NULL));
	//std::random_device rd;
	////mt19937 gen;
	//std::minstd_rand gen;
	//gen.seed(0);
	//std::uniform_real_distribution<> dis(0.00, 0.999);

	//
	//x = 0; y = 0;
	//for (int i = 0; i < N; i++)
	//{
	//	a = rand() % 4 + 1;
	//	r = dis(gen);
	//	l = l_func(r);
	//	if (a == 1) x += l;
	//	if (a == 2) y += l;
	//	if (a == 3) x -= l;
	//	if (a == 4) y -= l;
	//	if (l > 8) l_arr[i] = 8.;
	//	else l_arr[i] = l;
	//}
	////std::copy(l_arr.begin(),l_arr.end(), std::ostream_iterator<double>(std::cout, " "));
	////std::cout << "\n";
	//std::sort(l_arr.begin(), l_arr.end(), std::greater<double>());
	////std::copy(l_arr.begin(), l_arr.end(), std::ostream_iterator<double>(std::cout, " "));
	////std::cout << "\n";
	//double h = (l_arr[0] - l_arr[N - 1]) / segment;
	//std::cout << "H = " << h << "\n";
	//std::vector<int> count(segment);
	//for (int i = 0; i < segment; i++)
	//{
	//	for (int j = 0; j < l_arr.size(); j++)
	//	{
	//		if ((l_arr[j] < l_arr[N - 1] + h * (i+1)) && l_arr[j] >= (l_arr[N - 1] + h * i))
	//		{
	//			count[i] ++;
	//		}
	//	}
	//}
	//std::ofstream xout1("tracexFl.txt");
	//for (int i = 0; i < segment; i++)
	//{
	//	xout1 << i * h + h/2<< "\t" << count[i]/(N*0.01)/28 << "\n";
	//}
	//xout1.close();

	std::ofstream expxout("exp_x.txt");
	std::ofstream expyout("exp_y.txt");
	std::ofstream dispxout("disp_x.txt");
	std::ofstream dispyout("disp_y.txt");
	std::ofstream displacout("displac.txt");
	expxout << 0 << " " << 0 << "\n";
	expyout << 0 << " " << 0 << "\n";
	dispxout << 0 << " " << 0 << "\n";
	dispyout << 0 << " " << 0 << "\n";
	displacout << 0 << " " << 0 << "\n";
	srand(time(NULL));
	std::random_device rd;
	//std::mt19937 gen;
	std::minstd_rand gen;
	gen.seed(0);
	std::uniform_real_distribution<> dis(0.00, 0.999);

	double x, y;
	std::ofstream fout("trace.txt");
	double exp_x = 0, exp_y = 0;
	double disp_x = 0, disp_y = 0;
	double displac = 0;
	for (int j = 0; j < p; j++)
	{
		exp_x = 0, exp_y = 0;
		disp_x = 0, disp_y = 0;
		displac = 0;
		for (int k = 0; k < M; k++)
		{
			x = 0; y = 0;
			for (int i = 0; i < N; i++)
			{
				/*a = dis(gen);
				r = dis(gen);
				l = l_func(r);
				if (a >= 0  && a < 0.25) x += l;
				if (a >= 0.25 && a < 0.50) y += l;
				if (a >= 0.50 && a < 0.75) x -= l;
				if (a >= 0.75 && a < 1) y -= l;*/
				a = rand() % 4 + 1;
				r = dis(gen);
				l = l_func(r);
				if (a == 1) x += l;
				if (a == 2) y += l;
				if (a == 3) x -= l;
				if (a == 4) y -= l;
				/*if (j == 0 ) {
					fout << x << " " << y << "\n";
				}*/
			}

			exp_x = exp_x + x;
			exp_y = exp_y + y;
			disp_x = disp_x + x * x;
			disp_y = disp_y + y * y;
		}

		exp_x = exp_x / M; exp_y = exp_y / M;
		disp_x = disp_x / M; disp_y = disp_y / M;

		disp_x = disp_x - exp_x * exp_x;
		disp_y = disp_y - exp_y * exp_y;
		displac = disp_x + disp_y;
		R[j] = displac;

		expxout << N << " " << exp_x << "\n";
		expyout << N << " " << exp_y << "\n";
		dispxout << N << " " << disp_x << "\n";
		dispyout << N << " " << disp_y << "\n";
		displacout << N << " " << displac << "\n";
		std::cout << j << "\t"  << N << "\n";
		//N += steps;
		N *= 10;
	}
	fout.close();
	expxout.close();
	expyout.close();
	dispxout.close();
	dispyout.close();
	displacout.close();

	MNK(steps, p, R, N_start);
	
	return 0;
}