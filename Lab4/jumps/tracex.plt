width=0.26
set xrange [0:]
set key left
set boxwidth width
set style fill solid 1.00 noborder
set grid
plot 'tracexFl.txt' u ($1):($2) w boxes lt rgb "#ff6200", 2/(x+1)**3 with lines lw 2 lt rgb 'black' title 'fl'
pause 50