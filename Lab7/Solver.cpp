#include "Solver.h"
template<typename value_t, typename ind_t, typename steps_t>
Solver<value_t, ind_t, steps_t>::Solver(std::map<std::string, steps_t>& lborders,
	std::map<std::string, steps_t>& h,
	const steps_t& epsilon, const steps_t& k, const steps_t& y0, const ind_t& N,
	bool fwrite)
{
		_lborders = lborders;
		_h = h;
		_epsilon = epsilon;
		_k = k;
		_y0 = y0;
		_series_size = N;
		_fwrite = fwrite;
		std::cout << "\nEpsilon " << _epsilon << "\nk = " << _k << "\n";
}


template<typename value_t, typename ind_t, typename steps_t>
void Solver<value_t, ind_t, steps_t>::Sheme(Eigen::Matrix<value_t, Eigen::Dynamic, 1> & res_vect,
	const ind_t& Nx, const ind_t& Ny,
	value_t(*phi)(steps_t),
	value_t(*func_x0)(steps_t, steps_t,steps_t, ind_t), value_t(*func_y0)(steps_t, steps_t),
	value_t(*func_yN)(steps_t, steps_t))
{
	clock_t t_total;
	t_total = clock();
	_size_mtrx = (Nx - 1) * (Ny - 1);

	auto alpha = [&]() {
		return (1. / (_h["x"] * _h["x"]));
	};
	auto betta = [&]() {
		return (1. / (_h["y"] * _h["y"]));
	};
	auto gamma = [&](ind_t i, steps_t y) {
		//std::cout << "\nEpsilon" << _epsilon << "\n k = " << _k << "\ny = " << y << "\n";
		return -2. * alpha() - 2. * betta() + (_k * _k * (1. + _epsilon * phi(y)));
	};

	Eigen::Matrix<value_t, Eigen::Dynamic, 1> right = Eigen::Matrix<value_t, Eigen::Dynamic, 1>::Zero(_size_mtrx, 1);
	std::vector<Eigen::Triplet<value_t>> coeff;
	Eigen::SparseMatrix<value_t> SpM(_size_mtrx, _size_mtrx);

	for (ind_t j = 0; j < Ny - 1; j++)
	{
		right(j) = -alpha() * func_x0(_lborders["x"], (j + 1) * _h["y"] + _lborders["y"], _y0, _series_size);// ������ ������.
		//��� Ux(x = Nx,y)) ���������������
		//right((_size_mtrx - 1) - (Ny - 2) + j) = -alpha() * func_x0(Nx * _h["x"], (j + 1) * _h["y"] + _lborders["y"], _y0, _series_size); // ��������� ������. 
	}
	for (ind_t i = 0; i < (Nx - 1); i++)
	{
		// 2 �������
		right(i * (Ny - 1)) += -betta() * func_y0((i + 1) * _h["x"] + _lborders["x"], _lborders["y"]);
		right(((i + 1) * (Ny - 1)) - 1) += -betta() * func_yN((i + 1) * _h["x"] + _lborders["x"], Ny * _h["y"] + _lborders["y"]);
	}


	for (ind_t i = 0; i < _size_mtrx; i++)
	{
		value_t y = 1 + i % (Ny - 1);
		//std::cout << "y = " << y * _h["y"] + _lborders["y"] << "\n";
		coeff.push_back(Eigen::Triplet<value_t>(i, i, gamma(i, y * _h["y"] + _lborders["y"])));
	}

	//��� Ux(x = Nx,y)) ����������������.
	for (size_t i = (_size_mtrx - 1) - (Ny - 2); i < _size_mtrx; i++)
	{
		coeff.push_back(Eigen::Triplet<value_t>(i, i, alpha()));
		// ����������� ����������. ��� ����� ������ �� �� ����������� ��������� ������, ��������� gamma + alpha.
	}


	for (ind_t i = 0; i < _size_mtrx - (Ny - 1); i++)
	{
		coeff.push_back(Eigen::Triplet<value_t>(i, i + (Ny - 1), alpha()));
	}
	for (ind_t i = Ny - 1; i < _size_mtrx; i++)
	{
		coeff.push_back(Eigen::Triplet<value_t>(i, i - (Ny - 1), alpha()));
	}

	for (ind_t i = 0; i < _size_mtrx - 1; i++)
	{
		if ((i + 1) % (Ny - 1) != 0) coeff.push_back(Eigen::Triplet<value_t>(i, i + 1, betta()));
	}
	for (ind_t i = 1; i < _size_mtrx; i++)
	{
		if (i % (Ny - 1) != 0) coeff.push_back(Eigen::Triplet<value_t>(i, i - 1, betta()));
	}

	//std::cout << right << "\n";
	SpM.setFromTriplets(coeff.begin(), coeff.end());
	coeff.clear();
	//----------------------------------------
	double t_spm_start = clock();
	Eigen::SparseLU<Eigen::SparseMatrix<value_t>> dec;
	// Compute the ordering permutation vector from the structural pattern of A
	dec.analyzePattern(SpM);
	// Compute the numerical factorization 
	dec.factorize(SpM);

	//Eigen::SimplicialCholesky<Eigen::SparseMatrix<value_t>> chol(SpM); 

	double t_spm_end = clock() - t_spm_start;
	//----------------------------------------
	double t_solv_start = clock();
	//res_vect = std::move(chol.solve(right));
	res_vect = std::move(dec.solve(right));
	//res_vect = dec.solve(right);
	double t_solv_end = clock() - t_solv_start;
	//----------------------------------------
	right.resize(0, 0);
	// write file	
	std::string output = "Without write file";
	if (_fwrite) {
		std::ofstream fout(_PATH + "\\helmglotz.csv");
		fout << "x;y;U\n";
		output = "With write file";
		// fout for x = x0, y = y0
		fout << _lborders["x"] << ";" << _lborders["y"]
				<< ";" << static_cast<value_t>(0.) << "\n";
		// fout for x = x0, y0 < y < y1
		for (ind_t j = 0; j < Ny - 1; j++)
		{
			fout << _lborders["x"] << ";" << (j + 1) * _h["y"] + _lborders["y"]
				<< ";" << func_x0(_lborders["x"], (j + 1) * _h["y"] + _lborders["y"], _y0, _series_size) << "\n";
		}

		// fout for x = x0, y = y1
		fout << _lborders["x"] << ";" << Ny * _h["y"]
			<< ";" << static_cast<value_t>(0.) << "\n";

		for (ind_t i = 0; i < Nx - 1; i++)
		{
			// fout for x0 < x < x1, y = y0
			fout << (i + 1) * _h["x"] + _lborders["x"] << ";" << _lborders["y"]
				<< ";" << func_y0((i + 1) * _h["x"] + _lborders["x"], _lborders["y"]) << "\n";

			for (ind_t j = 0; j < Ny - 1; j++)
			{
				fout << (i + 1) * _h["x"] + _lborders["x"] << ";" << (j + 1) * _h["y"] + _lborders["y"]
					<< ";" << res_vect(i * (Ny - 1) + j) << "\n";
			}

			// fout for x0 < x < x1, y = y1
			fout << (i + 1) * _h["x"] + _lborders["x"] << ";" << Ny * _h["y"]
				<< ";" << func_yN((i + 1) * _h["x"] + _lborders["x"], Ny * _h["y"]) << "\n";
		}
		//fout for x = x1, y = y0
		fout << Nx * _h["x"] << ";" << _lborders["y"]
			<< ";" << static_cast<value_t>(0.) << "\n";

		// fout for x = x1, y0 < y < y1
		for (ind_t j = 0; j < Ny - 1; j++)
		{
			fout << Nx * _h["x"] << ";" << (j + 1) * _h["y"] + _lborders["y"]
				<< ";" << res_vect(_size_mtrx - (Ny - 1) + j) << "\n";
		}

		// fout for x = x1, y = y1
		fout << Nx * _h["x"] << ";" << Ny * _h["y"]
			<< ";" << static_cast<value_t>(0.) << "\n";
		fout.close();
	}
	//----------------------------------------
	t_total = clock() - t_total;
#ifndef NDEBUG
	std::cout << "Time SparseLU: " << t_spm_end << " clicks (" << ((double)t_spm_end) / CLOCKS_PER_SEC << " seconds).\n";
	std::cout << "Time Solve: " << t_solv_end << " clicks (" << ((double)t_solv_end) / CLOCKS_PER_SEC << " seconds).\n";
#endif //  NDEBUG
	std::cout << "Time Sheme : (" << output << "): " << t_total << " clicks (" << ((double)t_total) / CLOCKS_PER_SEC << " seconds).\n";
	WriteParam(Nx, Ny, "helmgoltz");
}


template<typename value_t, typename ind_t, typename steps_t>
void Solver<value_t, ind_t, steps_t>::WriteParam(const ind_t& Nx, const ind_t& Ny, const std::string& method_name) noexcept
{
	std::ofstream param(_PATH + "\\param_" + method_name + ".csv");
	param << "epsilon;k;y0;N\n";
	param << _epsilon << ";" << _k << ";" << _y0 << ";" << _series_size;
	param.close();
}