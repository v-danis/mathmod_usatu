﻿#if 0
#define _USE_MATH_DEFINES
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <string>
#include <filesystem>
#include "Solver.h"
#include "Solver.cpp"
namespace fs = std::filesystem; // c++17 и более
double func_x0(double x, double y, double y0, uint64_t sN) {
	
	double u = 0;
	for (uint64_t i = 0; i <= sN; i++) u += sin(i * y) * sin(i * y0);
	u = u * (2. / sN);
	return u;

}

double func_y0(double x, double y) {
	return 0.;
}
double func_yN(double x, double y) {
	return 0.;
}
double phi(double y) {
	//std::cout << "y = " << y << "\n";
	return y * (1 - y) * (1. / 2. - y) * (1. / 2. - y);
	//return (1. / 2. - y) * (1. / 2. - y) * sin(M_PI * y);
}

bool demo_exists(const fs::path& p, fs::file_status s = fs::file_status{})
{
	//std::cout << p;
	if (fs::status_known(s) ? fs::exists(s) : fs::exists(p))
	{
		//std::cout << " exists\n";
		return true;
	}
	else
	{
		//std::cout << " does not exist\n";
		return false;
	}
}

int main()
{
	if (!demo_exists("Data")) {
		fs::create_directories("Data");
	}
	setlocale(LC_ALL, "ru");

	double epsilon = 1., k = 15., y0 = 0.5;
	uint64_t sN = 10000; // число членов тригонометрического ряда
	double left_border_x = 0., right_border_x = 10.;
	double left_border_y = 0., right_border_y = 1.;

	double hx = 0.01, hy = 0.01;

	std::map<std::string, double> h_m;
	h_m["x"] = hx;
	h_m["y"] = hy;

	//----------------------------------------
	uint64_t Nx = 0, Ny = 0;
	Nx = (right_border_x - left_border_x) / hx;
	Ny = (right_border_y - left_border_y) / hy;
	std::cout << "Nx = " << Nx << "\t" << "Ny = " << Ny << "\n";
	//---------------------------------------- 
	uint64_t x_vector_size = Nx + 1; // Last index for x = x_vector_size - 1.
	uint64_t y_vector_size = Ny + 1; // Last index for y = y_vector_size - 1.
	uint64_t size_mtrx = (Nx - 1) * (Ny - 1);
	//----------------------------------------
	std::cout << "Size U = " << size_mtrx * sizeof(double) * 1e-9 << " GB" << "\n";
	//----------------------------------------


	std::map<std::string, double> left_m;
	left_m["x"] = left_border_x;
	left_m["y"] = left_border_y;

	Eigen::Matrix<double, Eigen::Dynamic, 1> U(size_mtrx);
	Solver<double, uint64_t, double> wave(left_m,h_m, epsilon, k, y0, sN, true);
	wave.Sheme(U,
		Nx,Ny,
		phi,
		func_x0, func_y0,
		func_yN);
	return 0.;
}
#endif
	
#if 0
#define _USE_MATH_DEFINES
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <string>
#include <filesystem>

int main() {

	int Nx = 4, Ny = 4;

	int _size_mtrx = (Nx - 1) * (Ny - 1);

	auto alpha = [&]() {
		return 3.;
	};
	auto betta = [&]() {
		return 2.;
	};
	auto gamma = [&]() {
		//std::cout << "\nEpsilon" << _epsilon << "\n k = " << _k << "\ny = " << y << "\n";
		return 1.;
	};

	Eigen::Matrix<std::string, Eigen::Dynamic, 1> right(_size_mtrx, 1);
	std::vector<Eigen::Triplet<double>> coeff;
	Eigen::SparseMatrix<double> SpM(_size_mtrx, _size_mtrx);



	for (int j = 0; j < Ny - 1; j++)
	{
		right(j) = "-Alpha * U(0,y)";// Первая строка.
		//При Ux(x = Nx,y)) закоментировать
		//right((_size_mtrx - 1) - (Ny - 2) + j) = -alpha() * func_x0(Nx * _h["x"], (j + 1) * _h["y"] + _lborders["y"], _y0, _series_size); // Последняя строка. 
	}
	for (int i = 0; i < (Nx - 1); i++)
	{
		// 2 условия
		right(i * (Ny - 1)) += "-Betta * U(x,0)";
		right(((i + 1) * (Ny - 1)) - 1) += "-Betta * U(x,1)";
	}


	for (int i = 0; i < _size_mtrx; i++)
	{
		double y = 1 + i % (Ny - 1);
		//std::cout << "y = " << y << "\n";
		coeff.push_back(Eigen::Triplet<double>(i, i, gamma()));
	}

	//При Ux(x = Nx,y)) РАСКОМЕНТИРОВАТЬ.
	for (size_t i = (_size_mtrx - 1) - (Ny - 2); i < _size_mtrx; i++)
	{
		coeff.push_back(Eigen::Triplet<double>(i, i, alpha()));
		// Особенность библиотеки. При такой записи он не перезапишет последние строки, получится gamma + alpha.
	}


	for (int i = 0; i < _size_mtrx - (Ny - 1); i++)
	{
		coeff.push_back(Eigen::Triplet<double>(i, i + (Ny - 1), alpha()));
	}
	for (int i = Ny - 1; i < _size_mtrx; i++)
	{
		coeff.push_back(Eigen::Triplet<double>(i, i - (Ny - 1), alpha()));
	}

	for (int i = 0; i < _size_mtrx - 1; i++)
	{
		if ((i + 1) % (Ny - 1) != 0) coeff.push_back(Eigen::Triplet<double>(i, i + 1, betta()));
	}
	for (int i = 1; i < _size_mtrx; i++)
	{
		if (i % (Ny - 1) != 0) coeff.push_back(Eigen::Triplet<double>(i, i - 1, betta()));
	}


	SpM.setFromTriplets(coeff.begin(), coeff.end());
	std::cout << SpM << "\n";
	std::cout << right;

	return 0;
}
#endif
#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <string>
#include <iomanip>
#include <math.h>

using namespace std;

int N_global = 10000;
double eps = 0.00001;
double L = 9;
double k = 20;
double y_0 = 0.5;

int N_x = 10 * L;
int N_y = 50;

double phi(double y)
{
	return y * (1. - y) * (0.25 - y) * (0.25 - y);
}

double f(double y)
{
	double F = 0;
	for (int i = 0; i <= N_global; i++)
	{
		F += sin(i * y) * sin(i * y_0);
	}
	return F * 2.0 / (double)N_global;
}

void gauss(double** a, double* y, double* x, int N)
{
	double** A = (double**)malloc(N * sizeof(double*));
	for (int i = 0; i < N; i++)
	{
		A[i] = (double*)malloc(N * sizeof(double));
	}
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			A[i][j] = a[i][j];
		}
	}
	double* B = (double*)malloc(N * sizeof(double));
	for (int i = 0; i < N; i++)
	{
		B[i] = y[i];
	}
	int i, j, k;
	double p;
	bool b;
	for (i = 0; i < N - 1; i++)
	{
		if (A[i][i] == 0)
		{
			j = i + 1;
			b = false;
			while (b == false)
			{
				if (!(A[j][i] == 0))
				{
					for (k = 0; k < N; k++)
					{
						p = A[i][k];
						A[i][k] = A[j][k];
						A[j][k] = p;
					}
					p = B[i];
					B[i] = B[j];
					B[j] = p;
					b = true;
				}
				j++;
			}
		}
		for (j = i + 1; j < N; j++)
		{
			p = A[j][i] / A[i][i];
			for (k = i; k < N; k++)
			{
				A[j][k] = A[j][k] - A[i][k] * p;
			}
			B[j] = B[j] - B[i] * p;
		}
	}
	for (i = 0; i < N; i++)
	{
		p = A[i][i];
		for (j = i; j < N; j++)
		{
			A[i][j] = A[i][j] / p;
		}
		B[i] = B[i] / p;
	}
	for (i = 0; i < N; i++)
	{
		x[i] = 0;
	}
	x[N - 1] = B[N - 1];
	for (i = N - 2; i >= 0; i--)
	{
		x[i] = B[i];
		for (j = i + 1; j < N; j++)
		{
			x[i] = x[i] - A[i][j] * x[j];
		}
	}
}

int main()
{
	double X = L, Y = 1;
	double dX = 0, dY = 0;
	double hX = 0, hY = 0;
	int N = N_x * N_y;

	dX = X / double(N_x);
	dY = Y / double(N_y);
	hX = 1. / (double)pow(dX, 2);
	hY = 1. / (double)pow(dY, 2);

	double** M, * A, * B;
	M = new double* [N];
	A = new double[N];
	B = new double[N];

	for (int i = 0; i < N; i++)
	{
		M[i] = new double[N];
		A[i] = 0;
		B[i] = 0;
	}
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			if (i == j)
			{
				M[i][j] = 1;
			}
			else
			{
				M[i][j] = 0.0;
			}
		}
	}
	int in = N_y;
	do
	{
		if (in < N_y * (N_x - 1))
		{
			if ((in % N_x == 0) || ((in + 1) % N_x == 0))
			{
				M[in][in - 1] = M[in][in + 1] = 0;
				M[in][in - N_y] = M[in][in + N_y] = 0;
				M[in][in] = 1;
			}
			else
			{
				M[in][in] = -2. * hX - 2. * hY + pow(k, 2) * (1 + eps * phi(in * dY));
				M[in][in - 1] = M[in][in + 1] = hY;
				M[in][in - N_y] = M[in][in + N_y] = hX;
			}
		}
		else
		{
			M[in][in - 1] = -1;
		}
		in++;
	} while (in < N);
	for (int i = 0; i < N_y; i++)
	{
		B[i] = f(i * dY);
	}
	cout << endl;
	ofstream fout;
	gauss(M, B, A, N);
	fout.open("Data/data.txt");
	int iter = 0;
	for (int i = 0; i < N_x; i++)
	{
		for (int j = 0; j < N_y; j++)
		{
			fout << A[iter] << "\t";
			iter++;
		}
		fout << endl;
	}
	fout << endl;
	fout.close();
	cout << "> " << "N = " << N_global << ", eps = " << eps << ", L = " << L << ", k = " << k << ", y0 = " << y_0 << endl;
	return 0;
}
