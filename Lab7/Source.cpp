
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
using namespace std;
#define M_PI 3.1415926535897932384626433832795

int N_x = 50; int N_y = 50;
double k = 15,
epsilon = 0.00001,
y_0 = 0.5;

bool Gauss(double** Aa, double* Bb, double* x, const int& number) 
{
	try
	{
		double** A = (double**)malloc(number * sizeof(double*));
		for (int i = 0; i < number; i++)
			A[i] = (double*)malloc(number * sizeof(double));

		for (int i = 0; i < number; i++)
			for (int j = 0; j < number; j++)
				A[i][j] = Aa[i][j];

		double* B = (double*)malloc(number * sizeof(double));
		for (int i = 0; i < number; i++)
			B[i] = Bb[i];

		int i, j, k;
		double flag;
		bool b;
		bool Neodnorodnaya = true;
		for (i = 0; i < number - 1; i++)
		{
			if (A[i][i] == 0) 
			{
				j = i + 1;
				b = false;
				while (b == false)
				{
					if (!(A[j][i] == 0))
					{
						for (k = 0; k < number; k++)
						{
							flag = A[i][k];
							A[i][k] = A[j][k];
							A[j][k] = flag;
						}
						flag = B[i];
						B[i] = B[j];
						B[j] = flag;
						b = true;
					}
					j++;
					if (j > number)
					{
						cout << "It is neodnorodnaya systema" << endl;
						Neodnorodnaya = false;
						return Neodnorodnaya;
					}
				}
			}
			for (j = i + 1; j < number; j++) 
			{
				flag = A[j][i] / A[i][i];
				for (k = i; k < number; k++)
					A[j][k] = A[j][k] - A[i][k] * flag;
				B[j] = B[j] - B[i] * flag;
			}
		}

		for (i = 0; i < number; i++) 
		{
			flag = A[i][i];
			for (j = i; j < number; j++)
			{
				A[i][j] = A[i][j] / flag;
			}
			B[i] = B[i] / flag;
		}

		for (i = 0; i < number; i++)
			x[i] = 0;
		x[number - 1] = B[number - 1];
		for (i = number - 2; i >= 0; i--)
		{
			x[i] = B[i];
			for (j = i + 1; j < number; j++)
			{
				x[i] = x[i] - A[i][j] * x[j];
			}
		}
		return Neodnorodnaya;
	}
	catch (exception& e)
	{
		cout << "ERROR at Gauss\n";
		cout << e.what() << endl;
		return false;
	}
}
double function(double y)
{
	//int N_f = 10000;
	int N_f = 10000;
	double result = 0;
	for (int i = 0; i <= N_f; i++)
		result += sin(i * y_0) * sin(i * y);
	result = result * 2 / (double)N_f;
	return result;
}

double phi(double y)
{
	double result;
	result = y * (1 - y) * (0.5 - y) * (0.5 - y);
	//result = sin(3.14*y);
	return result;
}

void print(double* A, int N_x, int N_y, FILE* pFile)
{
	int I = 0;
	for (int i = 0; i < N_y; i++)
	{
		for (int j = 0; j < N_x; j++)
		{
			fprintf(pFile, "%1.6lf	", A[I]);
			I++;
		}
		fprintf(pFile, "\n");
	}
	fprintf(pFile, "\n\n");
}

int main()
{
	FILE* pFile;

	double X_L, Y, delta_x, delta_y, coeff1, coeff2;
	double** matrix, * U, * F;

	X_L = 10;
	//X = 1;
	Y = 1;

	delta_x = X_L / N_x;
	delta_y = Y / N_y;

	coeff1 = 1. / delta_x / delta_x;
	coeff2 = 1. / delta_y / delta_y;

	matrix = (double**)malloc(sizeof(double*) * N_y * N_x);
	for (int i = 0; i < N_y * N_x; i++)
		matrix[i] = (double*)malloc(sizeof(double) * N_y * N_x);
	U = (double*)malloc(sizeof(double) * N_y * N_x);
	F = (double*)malloc(sizeof(double) * N_y * N_x);

	for (int i = 0; i < N_y * N_x; i++)
		for (int j = 0; j < N_y * N_x; j++)
			matrix[i][j] = 0;


	for (int i = 0; i < N_x * N_y; i++)
		matrix[i][i] = 1;

	for (int i = N_y; i < N_y * (N_x - 1); i++)
	{
		if ((i % N_x == 0) || ((i + 1) % N_x == 0))
		{
			matrix[i][i] = 1;
			matrix[i][i - 1] = matrix[i][i + 1] = 0;
			matrix[i][i - N_y] = matrix[i][i + N_y] = 0;
		}
		else
		{
			matrix[i][i] = -2. / delta_x / delta_x + -2. / delta_y / delta_y + k * k * (1 + epsilon * phi(i * delta_y));
			matrix[i][i - 1] = matrix[i][i + 1] = coeff2;
			matrix[i][i - N_y] = matrix[i][i + N_y] = coeff1;
		}

	}
	for (int i = N_y * (N_x - 1); i < N_y * N_x; i++)
		matrix[i][i - 1] = -1;

	for (int i = 0; i < N_x * N_y; i++)
		F[i] = 0;

	for (int i = 0; i < N_y; i++)
		F[i] = function(i * delta_y);


	Gauss(matrix, F, U, N_y * N_x);
	for (int i = 0; i < N_y; i++) cout << U[i * N_y] << "   " << U[(i + 1) * N_x - 1] << endl;
	fopen_s(&pFile, "data.txt", "w");
	print(U, N_y, N_x, pFile);
	fclose(pFile);

	return 0;
}