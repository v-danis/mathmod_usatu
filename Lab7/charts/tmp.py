import matplotlib.pyplot as plt
import numpy as np

xpoints = np.arange(0,8,0.01)
ypoints = np.arange(0,1,0.01)

u = ypoints*(1-ypoints)*(1/2 - ypoints)**2
plt.xlabel("Y")
plt.ylabel("U")
plt.plot(ypoints, u)
plt.show()