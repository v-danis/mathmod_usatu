import numpy as np
import pandas as pd
import plotly
import plotly.graph_objects as go

import pyqtgraph as pg
import pyqtgraph.opengl as gl
from pyqtgraph import functions as fn
from pyqtgraph.Qt import QtCore

from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
from matplotlib import cm
import plotly.express as px
import os

def plot_qt(_PATH):
    data = np.transpose(pd.read_csv(_PATH[0], sep=";").to_numpy())
    x,y, wave_d = data
    
    epsilon, k, y0, sN = np.transpose(pd.read_csv(_PATH[1], sep=";").to_numpy())
    

    app = pg.mkQApp("What")
    w = gl.GLViewWidget()
    w.show()
    w.setWindowTitle('What')
    w.setCameraPosition(distance=10)
    g = gl.GLGridItem()
    w.addItem(g)

    #sp = gl.GLScatterPlotItem(pos=np.column_stack((x,y,wave_d)), size=0.05, pxMode=False)
    sp = gl.GLSurfacePlotItem(x=x, y=y, z=wave_d, shader='normalColor')
    w.addItem(sp)

    pg.exec()
    
def plot_mpl(_PATH):
    data = np.transpose(pd.read_csv(_PATH[0], sep=";").to_numpy())
    x,y, wave_d = data
    epsilon, k, y0, sN = np.transpose(pd.read_csv(_PATH[1], sep=";").to_numpy())
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    ax.set_xlabel("X")
    ax.set_ylabel("Y")  
    ax.set_zlabel("U")
    surf = ax.plot_trisurf(x,y,wave_d, cmap=cm.Spectral)
    #fig.colorbar(surf,location="bottom")
    plt.show()


def plot_plotly(_PATH):
    #import plotly.io as pio
    #pio.renderers.default = "jpg"
    data = pd.read_csv(_PATH[0], sep=';')
    epsilon, k, y0, sN = np.transpose(pd.read_csv(_PATH[1], sep=";").to_numpy())
    epsilon = epsilon.item()
    k = k.item()
    y0 = y0.item()
    sN = sN.item()
    title = "k = " + str(k) + "\t" + "ε = " + str(epsilon) + "\t"  + " y0 = " + str(y0) + "\t"   + " sN = " + str(sN) 

    xdata = data['x'].values
    ydata = data['y'].values
    zdata = data['U'].values

    plotly.offline.plot({ "data": [go.Mesh3d(x=xdata, y=ydata, z=zdata,opacity=1,intensity=zdata, colorscale='spectral')],
                          "layout": go.Layout(title=title, margin=dict(l=65, r=50, b=65, t=90))
                          })
    #fig = go.Figure(data=[go.Mesh3d(x=xdata, y=ydata, z=zdata,opacity=1,intensity=zdata)])

    #fig.update_layout(title=title,  margin=dict(l=65, r=50, b=65, t=90))

    #fig.show()

if __name__ == "__main__":

    _PATH = [os.path.join(os.path.dirname(__file__),'..','Data','helmglotz.csv'),
             os.path.join(os.path.dirname(__file__),'..','Data','param_helmgoltz.csv'),
             os.path.join(os.path.dirname(__file__),'..','Data','data.npy')]
    #plot_qt(_PATH)
    #plot_mpl(_PATH)
    plot_plotly(_PATH)
    #data = np.load(_PATH[-1])
    #fig = px.imshow(data)
    #fig.show()
