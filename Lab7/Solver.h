#pragma once
#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <fstream>
#include <complex>
#include <sstream>
#include <ctime>
#include <cmath>
#include <Eigen/Dense>
#include <Eigen/Sparse>
template<typename value_t, typename ind_t, typename steps_t>
class Solver
{
public:
	std::map<std::string, steps_t> _h; // steps
	std::map<std::string, steps_t> _lborders; // _lborders["x"] == left borders x
	steps_t _epsilon;
	steps_t _k;
	steps_t _y0;
	ind_t _series_size;
private:
	ind_t _size_mtrx; 
	bool _fwrite; // write file flag
	std::string _PATH = "Data"; // file path
public:
	Solver();
	Solver(std::map<std::string, steps_t>& lborders,
		std::map<std::string, steps_t>& h,
		const steps_t& epsilon, const steps_t& k,const steps_t& y0, const ind_t& N,
		bool fwrite);
	void Sheme(Eigen::Matrix<value_t, Eigen::Dynamic, 1>& res_vect,
		const ind_t& Nx, const ind_t& Ny,
		value_t(*phi)(steps_t),
		value_t(*func_x0)(steps_t, steps_t, steps_t, ind_t), value_t(*func_y0)(steps_t, steps_t),
		value_t(*func_yN)(steps_t, steps_t));
	void WriteParam(const ind_t& Nx, const ind_t& Ny,const std::string& method_name) noexcept;
};