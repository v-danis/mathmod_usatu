from matplotlib import pyplot as plt
import matplotlib.animation as animation
import numpy as np
speed = 1
PATH = ["D:\\USATU\\7s\\MathMod\\Labs\\Lab3\\data\\Verle.dat","D:\\USATU\\7s\\MathMod\\Labs\\Lab3\\data\\simplexVerle.dat"]
velocity = np.loadtxt(PATH[0], unpack=False, dtype=float)


def animate(i):
    ax.clear()
    ax.set_ylim(-3,3)
    ax.plot(velocity[i * speed % len(velocity)], color='#4B0082')



fig, ax = plt.subplots()
print(np.min(velocity)*1.05, np.max(velocity)*1.05)
ani = animation.FuncAnimation(fig, animate, interval=10)

plt.show()